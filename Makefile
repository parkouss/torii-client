export PATH := $(PWD)/node_modules/.bin:$(PATH)

BROWSER=xdg-open

# .PHONY is used for targets that are not really files to build, so commands are
# executed in every case.
.PHONY: run-ws-test-server run-ws-test-client

out/proto_msgs.js: service.proto
	pbjs -t static-module -w commonjs -o $@ $^

out/proto_msgs.d.ts: out/proto_msgs.js
	pbts -o $@ $<

out/bundle.js: client.ts
	browserify $< --standalone client -p [ tsify --noImplicitAny ] > $@

out/bundle.min.js: out/bundle.js
	uglifyjs -o $@ $^

run-ws-test-server: out/proto_msgs.js
	./ws-test-server

run-ws-test-client: out/bundle.js
	${BROWSER} index-test.html
