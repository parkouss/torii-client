import { em } from "./out/proto_msgs.js"


function wsSend(ws: WebSocket, request: em.torii.Request)
{
    ws.send(em.torii.Request.encode(request).finish())
}

export {wsSend, em}
